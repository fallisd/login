A simple React and Redux application bootstrapped with Create React App.
This application was developed to explore user login and signout redirects as well as creating plots using React.js with and without the use of D3.js by creating various charts using live stock data from Alpha Vantage.
The backend for the application was bootstrapped using the Loopback Framework and runs seperately from the client. The backend was created jst to provide API routes for the login and sign up functinality of the application and is not particularly robust.


## Quickstart

Once the projected has been cloned or copied, the application will need two command windows to run the client and backend.

# Client

In the first command window, from the root directory of the project, the execute the following commands in sequence to start the client:

### `cd client`
### `npm install`
### `npm start`


Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Backend

In the second command window, from the root directory of the project, the execute the following commands in sequence to start the server:

### `cd backend`
### `npm install`
### `node .`
