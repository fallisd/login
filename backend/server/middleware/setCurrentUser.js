'use strict';

module.exports = function() {
  return function setCurrentUser(req, res, next) {
    if (!req.accessToken) {
      return next();
    }

    req.app.models.Account.findById(req.accessToken.userId, (err, user) => {
      if (err) {
        return next(err); 
      }
      if (!user)
        return next(new Error('No user with this access token was found.'));

      delete user.password;
      req.user = user;
      next();
    });
  }
}