'use strict';

var bluebird = require('bluebird');

module.exports = function(Account) {
  Account.signin = function(req, cb) {
    var err;
    
    if (!req.body.email) {
      err = new Error("Valid email required");
      err.statusCode = 417;
      err.code = "ACCOUNT_SIGNIN_FAILED_MISSING_REQUIREMENT_EMAIL";
      return cb(null, { status: "failure", field: "email", message: err.message, error: err });
    }
    if (!req.body.password) {
      err = new Error("Valid password required");
      err.statusCode = 417;
      err.code = "ACCOUNT_SIGNIN_FAILED_MISSING_REQUIREMENT_PASSWORD";
      return cb(null, { status: "failure", field: "password", message: err.message, error: err });
    }

    var email = req.body.email.toLowerCase();

    Promise.all([
      Account.findOne({
        where: { email: email },
      }),
      Account.login({
        email: email,
        password: req.body.password
      })
    ])
    .then(
      (data) => {
        var message = "Signed in user "+data[0].email
        return cb(null, { status: "success", user: data[0], token: data[1], message: message })
      },
      (err) => {
        console.log("Issue signing in user: "+err.message);
        return cb(null, { status: "failure", message: err.message, error: err });
      }
    )
  }

  Account.remoteMethod("signin", {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
    ],
    http: { path: "/signin", verb: "post" },
    returns: { arg: "data", type: "object" },
    description: "Logs in a user by creating and returning a token if a valid email and password are provided."      
  })

  Account.signup = function(req, cb) {
    var err;

    if (!req.body.email) {
      err = new Error("Valid email required");
      err.statusCode = 417;
      err.code = "ACCOUNT_SIGNUP_FAILED_MISSING_REQUIREMENT_EMAIL";
      return cb(null, { status: "failure", field: "email", message: err.message, error: err });
    }
    if (!req.body.password) {
      err = new Error("Valid password required");
      err.statusCode = 417;
      err.code = "ACCOUNT_SIGNUP_FAILED_MISSING_REQUIREMENT_PASSWORD";
      return cb(null, { status: "failure", field: "password", message: err.message, error: err });
    }

    var email = req.body.email.toLowerCase()

    Promise.all([
      Account.findOne({where: {email: email}})
    ])
      .then((results) => {
        var user = results[0]

        if (user) {
          err = new Error("Account already exists");
          err.statusCode = 422;
          err.code = "ACCOUNT_SIGNUP_FAILED_INVALID_REQUIREMENT_EMAIL"
          throw err;
        }
        
        return Account.create({
          email: email,
          password: req.body.password,
        })
      })
      .then(
        (result) => cb(null, {status:"success", user: result.user, message: "Created user "+email}), 
        (err) => cb(null, { status: "failure", message: err.message, error: err })
      )
  }

  Account.remoteMethod("signup", {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
    ],
    http: { path: "/signup", verb: "post" },
    returns: { arg: "data", type: "object" },
    description: "Creates a user given a valid email and password were provided."
  });

};
