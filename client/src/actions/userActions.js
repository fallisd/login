import * as ActionNames from 'utils/actionNames';
import {
  loginAPI,
  signupAPI,
} from 'api/userAPI';

function requestLogin() {
  return {
    type: ActionNames.REQUEST_LOGIN,
  };
}

function requestLoginSuccess({ userId, email, name }) {
  return {
    type: ActionNames.REQUEST_LOGIN_SUCCESS,
    userId,
    email,
    name,
  };
}

function requestLoginFail(message) {
  return {
    type: ActionNames.REQUEST_LOGIN_FAIL,
    message,
  };
}

export function login(email, password) {
  return async (dispatch) => {
    dispatch(requestLogin());
    if (!email) {
      dispatch(requestLoginFail('Email is required'));
      return;
    }
    if (!password) {
      dispatch(requestLoginFail('Password is required'));
      return;
    }
    try {
      const { accessToken, ...profile } = await loginAPI(email, password);
      localStorage.setItem('accessToken', accessToken);
      dispatch(requestLoginSuccess(profile));
    } catch (err) {
      dispatch(requestLoginFail(err.message));
    }
  };
}

function requestSignup() {
  return {
    type: ActionNames.REQUEST_SIGNUP,
  };
}

function requestSignupSuccess() {
  return {
    type: ActionNames.REQUEST_SIGNUP_SUCCESS,
  };
}

function requestSignupFail(message) {
  return {
    type: ActionNames.REQUEST_SIGNUP_FAIL,
    message,
  };
}

export function signUp(email, password, passwordTwo) {
  return async (dispatch) => {
    dispatch(requestSignup());
    if (!email) {
      dispatch(requestSignupFail('Email is required'));
      return;
    }
    if (!password) {
      dispatch(requestSignupFail('Password is required'));
      return;
    }
    if (!passwordTwo || password !== passwordTwo) {
      dispatch(requestSignupFail('Passwords do not match'));
      return;
    }
    try {
      await signupAPI(email, password);
      dispatch(requestSignupSuccess());
    } catch (err) {
      dispatch(requestSignupFail(err.message));
    }
  };
}

export function logout() {
  localStorage.setItem('accessToken', undefined);
  return {
    type: ActionNames.LOGOUT,
  };
}
