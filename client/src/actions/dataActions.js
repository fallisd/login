import * as ActionNames from 'utils/actionNames';
import {
  dataAPI,
} from 'api/dataAPI';

function requestData() {
  return {
    type: ActionNames.REQUEST_DATA,
  };
}

function requestDataSuccess(data, keys) {
  return {
    type: ActionNames.REQUEST_DATA_SUCCESS,
    data,
    keys,
  };
}

function requestDataFail(message) {
  return {
    type: ActionNames.REQUEST_DATA_FAIL,
    message,
  };
}

export function getData() {
  return async (dispatch) => {
    dispatch(requestData());
    try {
      const data = await dataAPI();
      const keys = Object.keys(data);
      dispatch(requestDataSuccess(data, keys));
    } catch (err) {
      dispatch(requestDataFail(err.message));
    }
  };
}

export function invalidateData() {
  return {
    type: ActionNames.INVALIDATE_DATA,
  };
}
