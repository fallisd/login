import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FullPageNoScroll } from 'components/pages/pages';
import TextInput from 'components/inputs/TextInput';
import Button from 'components/inputs/Button';
import Loading from 'components/loading/Loading';

import { login } from 'actions/userActions';

import './loginpage.css';


const propTypes = {
  dispatch: PropTypes.func.isRequired,
  message: PropTypes.string,
  loading: PropTypes.bool,
};

const defaultProps = {
  message: '',
  loading: false,
};

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  handleSubmit = () => {
    const { dispatch } = this.props;
    const { email, password } = this.state;
    dispatch(login(email, password));
  }

  handleChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  }

  renderMessage = () => {
    const { message } = this.props;
    if (message) {
      return (
        <div className='login-message'>
          {message}
        </div>
      );
    }
    return null;
  }

  checkLoading = () => {
    const { loading } = this.props;
    if (loading) {
      return (
        <div>
          <Loading />
        </div>
      );
    }
    return (
      <Fragment>
        <div className='login-logo-container'>
          <div className='login-logo'>
            {'LOGO'}
          </div>
        </div>
        <div className='login-form'>
          {'Email'}
          <TextInput
            onChange={(value) => { this.handleChange('email', value); }}
            onSubmit={this.handleSubmit}
          />
          {'Password'}
          <TextInput
            type='password'
            onChange={(value) => { this.handleChange('password', value); }}
            onSubmit={this.handleSubmit}
          />
        </div>
        <div className='login-button'>
          <Button
            text='LOGIN'
            onClick={this.handleSubmit}
          />
        </div>
      </Fragment>
    );
  }

  render() {
    const message = this.renderMessage();
    const display = this.checkLoading();
    return (
      <FullPageNoScroll>
        <div className='login-page-container'>
          {display}
          {message}
        </div>
        <div className='login-register-message'>
          {'Don’t have an account? \u00A0'}
          <Link to='/signup'>Sign Up</Link>
        </div>
      </FullPageNoScroll>
    );
  }
}

Page.propTypes = propTypes;
Page.defaultProps = defaultProps;

const mapStateToProps = state => ({
  loading: state.login.loading,
  message: state.login.message,
});


export default connect(mapStateToProps)(Page);
