import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { WidthLimitedPageWithBar } from 'components/pages/pages';
import Button from 'components/inputs/Button';

import './homepage.css';


const propTypes = {
  email: PropTypes.string.isRequired,
};

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { email } = this.props;
    return (
      <WidthLimitedPageWithBar>
        <div className='home-page-heading'>
          {`Hello ${email}`}
        </div>
        <div className='home-page-text'>
          {'Welcome to Stock Tracker Plotter. This application was developed to explore user login and signout redirects as well as creating plots using React.js with and without the use of D3.js by creating various charts using live stock data from Alpha Vantage.'}
        </div>
        <div className='home-page-text'>
          {'You can also explore using the menu in the top left corner.'}
        </div>
        <div className='home-page-button-menu'>
          <div className='home-page-button-container'>
            <i className='fas fa-chart-line fa-8x home-page-button-icon' />
            <Link to='/line' style={{ textDecoration: 'none', width: '200px' }}>
              <Button
                text='LINE'
              />
            </Link>
          </div>
          <div className='home-page-button-container'>
            <i className='far fa-chart-bar fa-8x home-page-button-icon' />
            <Link to='/bar' style={{ textDecoration: 'none', width: '200px' }}>
              <Button
                text='BAR'
              />
            </Link>
          </div>
          <div className='home-page-button-container'>
            <i className='fas fa-chart-pie fa-8x home-page-button-icon' />
            <Link to='/pie' style={{ textDecoration: 'none', width: '200px' }}>
              <Button
                text='PIE'
              />
            </Link>
          </div>
        </div>
      </WidthLimitedPageWithBar>
    );
  }
}

Page.propTypes = propTypes;

const mapStateToProps = state => ({
  email: state.user.email,
});

export default connect(mapStateToProps)(Page);
