import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FullPageNoScroll } from 'components/pages/pages';
import TextInput from 'components/inputs/TextInput';
import Button from 'components/inputs/Button';
import Loading from 'components/loading/Loading';

import { signUp } from 'actions/userActions';

import './loginpage.css';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  message: PropTypes.string,
  success: PropTypes.bool,
  loading: PropTypes.bool,
};

const defaultProps = {
  message: '',
  success: false,
  loading: false,
};


class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordTwo: '',
    };
  }

  handleSubmit = () => {
    const { dispatch } = this.props;
    const { email, password, passwordTwo } = this.state;
    dispatch(signUp(email, password, passwordTwo));
  }

  handleChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  }

  renderMessage = () => {
    const { message, success } = this.props;
    if (message) {
      if (success) {
        return (
          <div className='login-message-success'>
            {message}
          </div>
        );
      }
      return (
        <div className='login-message'>
          {message}
        </div>
      );
    }
    return null;
  }

  checkLoading = () => {
    const { loading } = this.props;
    if (loading) {
      return (
        <Loading />
      );
    }
    return (
      <Fragment>
        <div className='login-logo-container'>
          <div className='login-logo'>
            {'LOGO'}
          </div>
        </div>
        <div className='login-form'>
          {'Email'}
          <TextInput
            onChange={(value) => { this.handleChange('email', value); }}
            onSubmit={this.handleSubmit}
          />
          {'Password'}
          <TextInput
            type='password'
            onChange={(value) => { this.handleChange('password', value); }}
            onSubmit={this.handleSubmit}
          />
          {'Re-enter Password'}
          <TextInput
            type='password'
            onChange={(value) => { this.handleChange('passwordTwo', value); }}
            onSubmit={this.handleSubmit}
          />
        </div>
        <div className='login-button'>
          <Button
            text='SIGN UP'
            onClick={this.handleSubmit}
          />
        </div>
      </Fragment>
    );
  }

  render() {
    const message = this.renderMessage();
    const display = this.checkLoading();

    return (
      <FullPageNoScroll>
        <div className='login-page-container'>
          {display}
          {message}
        </div>
        <div className='login-register-message'>
          {'Already have an account? \u00A0'}
          <Link to='/login'>Login</Link>
        </div>
      </FullPageNoScroll>
    );
  }
}

Page.propTypes = propTypes;
Page.defaultProps = defaultProps;

const mapStateToProps = state => ({
  loading: state.signUp.loading,
  message: state.signUp.message,
  success: state.signUp.success,
});

export default connect(mapStateToProps)(Page);
