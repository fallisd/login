import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { WidthLimitedPageWithBar } from 'components/pages/pages';
import Button from 'components/inputs/Button';
import Loading from 'components/loading/Loading';
import Bar from 'components/charts/Bar';
import { getData } from 'actions/dataActions';

import './piepage.css';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  keys: PropTypes.arrayOf(PropTypes.string).isRequired,
};

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = () => {
    const { dispatch } = this.props;
    dispatch(getData());
  }

  display = () => {
    const {
      data,
      keys,
      loading,
      message,
    } = this.props;

    if (loading) {
      return (
        <Loading />
      );
    }
    if (message) {
      return (
        <div>
          {message}
        </div>
      );
    }

    const bardata = keys.slice(0, 5).map((key, index) => {
      let name = 'Today';
      if (index === 1) {
        name = 'Yesterday';
      } else if (index !== 0) {
        name = `${index} Days`;
      }
      return {
        value: data[key]['4. close'],
        name,
      };
    });

    const height = 400;
    const width = 800;

    return (
      <Bar
        data={bardata}
        width={width}
        height={height}
        xname='a'
        yname='b'
      />
    );
  }

  render() {
    const display = this.display();
    return (
      <WidthLimitedPageWithBar>
        <div className='home-page-heading'>
          {'Bar Graph'}
        </div>
        <div className='pie-page-text'>
          {'A bar graph updated with live data to show the current price of MSFT stock as well as its history over the last 4 days. The graph uses the D3.js library for data formatting but only declarative React components for displaying the graph as an SVG.'}
        </div>
        <div className='pie-page-text'>
          {'Alpha Vantage limits the number of API requests so refreshing the data too frequently will result in a delayed response.'}
        </div>
        <Button
          text='REFRESH DATA'
          onClick={this.handleClick}
        />
        <div className='table-page-display-container'>
          {display}
        </div>
      </WidthLimitedPageWithBar>
    );
  }
}

Page.propTypes = propTypes;

const mapStateToProps = state => ({
  data: state.data.data,
  keys: state.data.keys,
  loading: state.data.loading,
  message: state.data.message,
});

export default connect(mapStateToProps)(Page);
