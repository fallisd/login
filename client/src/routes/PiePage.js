import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { WidthLimitedPageWithBar } from 'components/pages/pages';
import Button from 'components/inputs/Button';
import Loading from 'components/loading/Loading';
import Slice from 'components/charts/Pie';
import { getData } from 'actions/dataActions';

import './piepage.css';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  keys: PropTypes.arrayOf(PropTypes.string).isRequired,
};

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = () => {
    const { dispatch } = this.props;
    dispatch(getData());
  }

  display = () => {
    const {
      data,
      keys,
      loading,
      message,
    } = this.props;

    if (loading) {
      return (
        <Loading />
      );
    }
    if (message) {
      return (
        <div>
          {message}
        </div>
      );
    }

    const pieData = keys.slice(0, 4).map(key => data[key]['4. close']);

    const height = 400;
    const width = 400;

    return (
      <svg height={height} width={width}>
        <g transform={`translate(${width / 2} ,${height / 2} )`}>
          <Slice data={pieData} />
        </g>
      </svg>
    );
  }

  render() {
    const display = this.display();
    return (
      <WidthLimitedPageWithBar>
        <div className='home-page-heading'>
          {'Pie Chart'}
        </div>
        <div className='pie-page-text'>
          {'A bar chart using recent MSFT stock prices (not a very useful format for visualizing this data). The graph uses the D3.js library for data formatting as well as manipulating the SVG. Not a huge fan of this method because of how D3.js is manipulating the DOM without React knowing. It seems to work fine but it should probably be done differently. Solutions on the web suggest that it should be done only after the component has been mounted using a lifecycle method.' }
        </div>
        <div className='pie-page-text'>
          {'Alpha Vantage limits the number of API requests so refreshing the data too frequently will result in a delayed response.'}
        </div>
        <Button
          text='REFRESH DATA'
          onClick={this.handleClick}
        />
        <div className='table-page-display-container'>
          {display}
        </div>
      </WidthLimitedPageWithBar>
    );
  }
}

Page.propTypes = propTypes;

const mapStateToProps = state => ({
  data: state.data.data,
  keys: state.data.keys,
  loading: state.data.loading,
  message: state.data.message,
});

export default connect(mapStateToProps)(Page);
