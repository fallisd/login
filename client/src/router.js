import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper';

import LoginPage from 'routes/LoginPage';
import SignupPage from 'routes/SignupPage';
import HomePage from 'routes/HomePage';
import PiePage from 'routes/PiePage';
import LinePage from 'routes/LinePage';
import BarPage from 'routes/BarPage';

const locationHelper = locationHelperBuilder({});

const userIsAuthenticated = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: state => state.user.userId !== undefined,
  wrapperDisplayName: 'UserIsAuthenticated',
});

const userIsNotAuthenticated = connectedRouterRedirect({
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',
  allowRedirectBack: false,
  authenticatedSelector: state => state.user.userId === undefined,
  wrapperDisplayName: 'UserIsNotAuthenticated',
});

export default (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={userIsAuthenticated(HomePage)} />
      <Route path='/pie' component={userIsAuthenticated(PiePage)} />
      <Route path='/line' component={userIsAuthenticated(LinePage)} />
      <Route path='/bar' component={userIsAuthenticated(BarPage)} />
      <Route path='/login' component={userIsNotAuthenticated(LoginPage)} />
      <Route path='/signup' component={userIsNotAuthenticated(SignupPage)} />
      <Redirect from='*' to='/' />
    </Switch>
  </BrowserRouter>
);
