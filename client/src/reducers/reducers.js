import { combineReducers } from 'redux';
import {
  login,
  signUp,
  user,
} from './userReducers';
import data from './dataReducers';

export default combineReducers({
  login,
  signUp,
  user,
  data,
});
