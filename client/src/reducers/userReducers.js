import * as ActionNames from 'utils/actionNames';

export function login(state = { loading: false, message: '' }, action) {
  switch (action.type) {
    case ActionNames.REQUEST_LOGIN:
      return Object.assign({}, state, {
        loading: true,
        message: '',
      });
    case ActionNames.REQUEST_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        message: '',
      });
    case ActionNames.REQUEST_LOGIN_FAIL:
      return Object.assign({}, state, {
        loading: false,
        message: action.message,
      });

    default:
      return state;
  }
}

export function signUp(state = { loading: false, message: '', success: false }, action) {
  switch (action.type) {
    case ActionNames.REQUEST_SIGNUP:
      return Object.assign({}, state, {
        loading: true,
        message: '',
        success: false,
      });
    case ActionNames.REQUEST_SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        message: 'Signup was successful',
        success: true,
      });
    case ActionNames.REQUEST_SIGNUP_FAIL:
      return Object.assign({}, state, {
        loading: false,
        message: action.message,
        success: false,
      });

    default:
      return state;
  }
}

export function user(state = {}, action) {
  switch (action.type) {
    case ActionNames.REQUEST_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        userId: action.userId,
        email: action.email,
        name: action.name,
      });
    case ActionNames.LOGOUT:
      return {};
    default:
      return state;
  }
}
