import * as ActionNames from 'utils/actionNames';


export default function data(state = {
  loading: true,
  invalid: true,
  data: {},
  message: '',
  keys: [],
},
action) {
  switch (action.type) {
    case ActionNames.INVALIDATE_DATA: {
      return Object.assign({}, state, {
        invalid: true,
        message: '',
      });
    }
    case ActionNames.REQUEST_DATA:
      return Object.assign({}, state, {
        loading: true,
        invalid: false,
        message: '',
      });
    case ActionNames.REQUEST_DATA_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        invalid: false,
        data: action.data,
        keys: action.keys,
        message: '',
      });
    case ActionNames.REQUEST_DATA_FAIL:
      return Object.assign({}, state, {
        loading: false,
        invalid: false,
        data: {},
        keys: [],
        message: action.message,
      });
    default:
      return state;
  }
}
