import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';

import Button from 'components/inputs/Button';
import { logout } from 'actions/userActions';

import './topbar.css';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
};

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
    };
  }

  handleMenuClick = () => {
    this.setState(prevState => ({
      menuOpen: !prevState.menuOpen,
    }));
  }

  handleLogoutClick = () => {
    const { dispatch } = this.props;
    dispatch(logout());
  }

  render() {
    const { menuOpen } = this.state;
    return (
      <div className='top-bar-container'>
        <div className='top-bar-left'>
          <Button
            className='top-bar-button'
            onClick={this.handleMenuClick}
            icon={<i className='fas fa-bars fa-2x top-bar-icon top-bar-hover' />}
          />
          <div className='top-bar-title'>
            {'Tracker Plotter'}
          </div>
        </div>
        <Button
          className='top-bar-button'
          onClick={this.handleLogoutClick}
          text='Logout'
        />
        <CSSTransition
          in={menuOpen}
          timeout={500}
          classNames='side-menu'
          unmountOnExit
        >
          <div className='side-menu'>
            <Link to='/' style={{ textDecoration: 'none' }}>
              <div className='side-menu-menuitem'>
                <i className='fas fa-home' />
                <div className='side-menu-menuitem-text'>
                  {'HOME'}
                </div>
              </div>
            </Link>
            <Link to='/line' style={{ textDecoration: 'none' }}>
              <div className='side-menu-menuitem'>
                <i className='fas fa-chart-line' />
                <div className='side-menu-menuitem-text'>
                  {'LINE'}
                </div>
              </div>
            </Link>
            <Link to='/bar' style={{ textDecoration: 'none' }}>
              <div className='side-menu-menuitem'>
                <i className='far fa-chart-bar' />
                <div className='side-menu-menuitem-text'>
                  {'BAR'}
                </div>
              </div>
            </Link>
            <Link to='/pie' style={{ textDecoration: 'none' }}>
              <div className='side-menu-menuitem'>
                <i className='fas fa-chart-pie' />
                <div className='side-menu-menuitem-text'>
                  {'PIE'}
                </div>
              </div>
            </Link>
          </div>
        </CSSTransition>
      </div>
    );
  }
}

TopBar.propTypes = propTypes;

export default connect()(TopBar);
