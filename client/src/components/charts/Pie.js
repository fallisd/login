import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

import './pie.css';

export default (props) => {
  const { data } = props;
  const xFormat = d3.format('d');
  const formattedData = data.map(value => xFormat(value));
  const dataPie = d3.pie()(formattedData);
  const interpolate = d3.interpolateRgb('#eaaf79', '#bc3358');

  return dataPie.map((slice, index) => {
    const sliceColor = interpolate(index / (dataPie.length - 1));
    return (
      <Path
        key={sliceColor}
        radius={150}
        slice={slice}
        sliceColor={sliceColor}
        value={formattedData[index]}
      />
    );
  });
};

const pathPropTypes = {
  radius: PropTypes.number.isRequired,
  slice: PropTypes.objectOf(PropTypes.any).isRequired,
  sliceColor: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

class Path extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };
  }

  handleMouseOver = () => {
    this.setState({
      hovered: true,
    });
  }

  handleMouseOut = () => {
    this.setState({
      hovered: false,
    });
  }

  render() {
    const {
      radius,
      slice,
      sliceColor,
      value,
    } = this.props;

    const { hovered } = this.state;
    const outerRadius = hovered ? radius * 1.1 : radius;
    const innerRadius = radius * 0.7;

    const dataArc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .padAngle(0.01)
      .cornerRadius(2);

    return (
      <g>
        <path
          d={dataArc(slice)}
          fill={sliceColor}
          onMouseOver={this.handleMouseOver}
          onMouseOut={this.handleMouseOut}
          onFocus={this.handleMouseOver}
          onBlur={this.handleMouseOut}
        />
        {hovered && (
          <circle r={innerRadius * 0.95} fill={sliceColor} />
        )}
        {hovered && (
          <text
            className='pie-center-text'
            dominantBaseline='middle'
            textAnchor='middle'
          >
            {value}
          </text>
        )}
      </g>
    );
  }
}

Path.propTypes = pathPropTypes;
