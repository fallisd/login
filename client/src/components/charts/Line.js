import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

import './line.css';

const propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  xname: PropTypes.string.isRequired,
  yname: PropTypes.string.isRequired,
};

function Line(props) {
  const {
    data,
    width,
    height,
    xname,
    yname,
  } = props;

  const margin = 20;
  const xFormat = d3.format('d');

  const adjustedHeight = height - 2 * margin;
  const adjustedWidth = width - 2 * margin;

  const x = d3.scaleLinear()
    .domain(d3.extent(data, d => d[xname]))
    .range([margin * 2, adjustedWidth]);

  const y = d3.scaleLinear()
    .domain([d3.min(data, d => d[yname]) - 1, d3.max(data, d => d[yname])])
    .range([adjustedHeight, margin]);

  const dataLine = d3.line()
    .x(d => x(d[xname]))
    .y(d => y(d[yname]))
    .curve(d3.curveCatmullRom.alpha(0.5));

  const xTicks = x.ticks(10).map(d => (
    x(d) <= adjustedWidth
      ? (
        <g transform={`translate(${x(d)},${height - margin})`} key={d}>
          <text>{xFormat(d)}</text>
          <line x1='0' x2='0' y1='0' y2='5' transform={`translate(0,-${margin})`} />
        </g>
      )
      : null
  ));

  const yTicks = y.ticks(6).map(d => (
    y(d) < adjustedHeight
      ? (
        <g transform={`translate(${margin},${y(d)})`} key={d}>
          <text>{xFormat(d)}</text>
          <line x1='0' x2='-5' y1='0' y2='0' transform={`translate(${margin},0)`} />
        </g>
      )
      : null
  ));

  return (
    <svg width={width} height={height}>
      <line className='line-axis' x1={margin * 2} x2={adjustedWidth} y1={adjustedHeight} y2={adjustedHeight} />
      <line className='line-axis' x1={margin * 2} x2={margin * 2} y1={margin} y2={adjustedHeight} />
      <path d={dataLine(data)} className='line-path' />
      <g className='line-axis-labels'>
        {xTicks}
      </g>
      <g className='line-axis-labels'>
        {yTicks}
      </g>
    </svg>
  );
}

Line.propTypes = propTypes;

export default Line;
