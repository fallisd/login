import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './textinput.css';

const propTypes = {
  type: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
};

const defaultProps = {
  type: 'text',
  onChange: () => {},
  onSubmit: () => {},
};

class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  handleChange = (value) => {
    const { onChange } = this.props;
    this.setState({ value });
    if (onChange) {
      onChange(value);
    }
  }

  handleKeyUp = (event) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      if (event.keyCode === 13) {
        onSubmit();
      }
    }
  }

  render() {
    const { type } = this.props;
    const { value } = this.state;
    return (
      <input
        className='text-input'
        type={type}
        spellCheck='false'
        value={value}
        onChange={(event) => { this.handleChange(event.target.value); }}
        onKeyUp={this.handleKeyUp}
      />
    );
  }
}

TextInput.propTypes = propTypes;
TextInput.defaultProps = defaultProps;
export default TextInput;
