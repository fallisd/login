import React from 'react';
import ReactDOM from 'react-dom';
import { FullPage, WidthLimitedFullPage, FullPageNoScroll } from './pages';

it('FullPage renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FullPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('WidthLimitedFullPage renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<WidthLimitedFullPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('FullPageNoScroll renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FullPageNoScroll />, div);
  ReactDOM.unmountComponentAtNode(div);
});