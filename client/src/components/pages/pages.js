/* eslint-disable react/prop-types */
import React from 'react';

import TopBar from 'components/bars/TopBar';

import './pages.css';

export const FullPage = ({ children }) => (
  <div className='full-page'>
    {children}
  </div>
);

export const WidthLimitedFullPage = ({ children }) => (
  <FullPage>
    <div className='width-limited-page'>
      {children}
    </div>
  </FullPage>
);

export const FullPageNoScroll = ({ children }) => (
  <div className='no-scroll-page'>
    {children}
  </div>
);

export const WidthLimitedPageWithBar = ({ children }) => (
  <div className='width-limited-container'>
    <TopBar />
    <div className='width-limited-page'>
      {children}
    </div>
  </div>
);
