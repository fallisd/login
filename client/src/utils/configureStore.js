import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import reducers from 'reducers/reducers';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default function configureStore() {
  const enhancers = compose(
    applyMiddleware(thunkMiddleware),
  );

  const store = createStore(
    persistedReducer,
    enhancers,
  );
  const persistor = persistStore(store);
  return { store, persistor };
}
