export async function loginAPI(email, password) {
  try {
    let res = await fetch(`${process.env.REACT_APP_API_ROOT}api/accounts/signin`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });
    res = await res.json();
    if (res && res.data && res.data.status === 'success') {
      return {
        accessToken: res.data.token,
        email: res.data.user.email,
        userId: res.data.user.id,
      };
    }
    throw Error('Email or password is incorrect');
  } catch (err) {
    throw err;
  }
}

export async function signupAPI(email, password) {
  try {
    let res = await fetch(`${process.env.REACT_APP_API_ROOT}api/accounts/signup`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });
    res = await res.json();
    if (!(res && res.data && res.data.status === 'success')) {
      throw Error(res.data.message);
    }
  } catch (err) {
    throw err;
  }
}
