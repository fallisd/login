export async function dataAPI() {
  try {
    let res = await fetch('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT&apikey=demo', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    res = await res.json();
    if (!(res && res['Time Series (Daily)'])) {
      throw Error('Error fetching data');
    }
    return res['Time Series (Daily)'];
  } catch (err) {
    throw err;
  }
}

export async function dataAPI2() {
  return {};
}
