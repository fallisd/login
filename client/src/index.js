import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import HttpsRedirect from 'react-https-redirect';
import { Provider } from 'react-redux';
// import { PersistGate } from 'redux-persist/integration/react';

import './index.css';

import configureStore from 'utils/configureStore';
import router from 'router';
// import * as serviceWorker from 'serviceWorker';

const { store } = configureStore();

class App extends Component {
  render() {
    return router;
  }
}

ReactDOM.render(
  <Provider store={store}>
    <HttpsRedirect>
      <App />
    </HttpsRedirect>
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
/*
ReactDOM.render(
  <Provider store={store}>
    <HttpsRedirect>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </HttpsRedirect>
  </Provider>,
  document.getElementById('root'),
);
*/
